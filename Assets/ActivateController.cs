﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateController : MonoBehaviour
{
    public CharacterController characterController;

    // Start is called before the first frame update
    void Start()
    {
        characterController.enabled = false;
        Invoke("activatecontroller", 24);
    }

    private void activatecontroller()
    {
        characterController.enabled = true;
    }
}
