﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTPSController : MonoBehaviour
{
    public UnityEvent onInteractionInput;
    public Camera cam;
    private InputData input;
    private CharacterAnimBasedMovement characterMovement;
    public bool onInteractionzone { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        input.getInput();
        

        if(onInteractionzone && input.jump)
        {
            onInteractionInput.Invoke();
        }
        else
        {
            characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash);
        }
    }
}
